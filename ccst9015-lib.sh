#!/bin/bash

#
# Name: ccst9015-lib.sh
#
# Author: C.-H. Dominic HUNG <dominic@teli.hku.hk / chdhung@hku.hk>
#  Technology-Enriched Learning Initiative,
#  The University of Hong Kong
#
# Description: This script pre-populates and/or updates local repositories of
#  student project library code for the course CCST9015 offered in Spring 2019.
#  The script will crawl updates if triggered to allow the running devices to
#  obtain the most up-to-date articles to be presented to user.
#
#  [WARNING] This script and the action script that handles Git-repositories
#  should be ran at root privilege to avoid unintentional and unintended modi-
#  fications on the loaded centrally-maintained libraries files.
#

# PARAMETERS
# ====================
#
PROJ_BASE="/usr/share/ccst9015"
PROJ_PRFX=
PROJ_NAME=([0]="lib")
        # If this array variable is specified, the script will overwrite the
        #  naming of the target directory of repositories by values of this
        #  variable. Automatically generated target directory name composed of
        #  values in variable PROJ_PRFX and an iteration number that starts
        #  counting from 0 will be used otherwise.
REPO_SEVR=([0]="github.com")
REPO_USER=([0]="CCST9015")
REPO_NAME=([0]="Library")
REPO_BRCH=()
ACTN_SCPT="./common/git-popup.sh"
SSHK_FILE=()
        # If a path to SSH key file, e.g., /etc/id_rsa, is provided through
        #  this variable, the default key interleaved below will not be used.
SSH_PRIVK="""-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAojBt8gb8Ss6Ejvf+STp4a34Dat3uiqTWpf7sI4WpyDRPUvxl
yZr6VuR8cB8/avefu1Ms3g+Y3uGboydQg0qyoalq45VNmtDvrwpGHdaDjWzhtUhy
FhNC79pIidXuKAW6GJp65eLsLuqi3bTPOn8QGb1tLi1HyD8svCABJ5st2CA/8MdW
9NcImUwnyr0SWAzIqPifv4rEzOW8g9tei9odVAyKg28jR7fw1qGTEmHWX1uD5nXm
/cAut1PDzycRd4dgSLS5NFsr0XEiGbXfPp9bO82FiwnIAORK3peS2FZ5zMvorWtw
wakWRy1ggGk2c5VwocSJvSEmcVKrczqwh/gVGQIDAQABAoIBAGwMb3JpjPIZGt5U
ACewuECENqqfJydeIVF8vrv7c9xYXPAgDX99q4DvvqbDanBMfzFAfAeDxIfXLORI
cK7GuLJSN09eS9DzmHm4EnYWTpWND+DBXp+uPU2k0eZhisNG7W8umulDf6zRy6Z/
qf6CX6uGMpIswEF7NMYvXBWSn7v20KJ4lk1eZntJ1N3lTP+TjhRu4XjE4tzv2+oZ
I5X4s9ax+D3MwsKDaUTAOWOvsiJIOshQ5gy+eR0Tlm+g4e4vl5++59jL0UqsggxP
BNELEjV9D+z/7iS50KGF9QE80MQPoOhSuqTxDeqajsI56xvV7KIIdu+LfZbLdcNh
MvAKcAECgYEAzHOqVwo1810X3a8Q/N/xHSJoUiZCI4i8ZJgK1A556pzlsM/RIn/q
kiHYBxXYWPmpOKjXzvrFInXe7GR/UcY2eCk2woZ6hTyY+f12F2dk3cFer40ytnoJ
qDOtZOGEZEq8CtR3g7wtYH++4Ki+oLL7XOLM8lqBTb/p0DoHDhhv14ECgYEAyxTs
cggzV72fExAwxPhT4SQ3MLqfFVpJPF/t0znHnpVwFHSFKXo0K4FYEGJ2LTJRhEEL
MBDWwPKZnsGmZxzL74PQMhD+9uWJ9PnV/yUSpXDGMZEzw3fYACi05pk0iDFaxdq2
yZWcD04H+WBUIDbZxn0mHkrZ724RLqHuAnLsyZkCgYBmJKtHjPxjN3FC+cG89Izb
h2MjF6YXZqrofuV6CDl0syo16b2MROgGVg49i/ZojWKzwfb/m1191/GlnJfXXPW9
lqmbCeCQTDQJQU4MZ0WHXKJXhm19qP7GH/tS2TBxgrzrlDaZhBwPiALu7p8+Xe0i
/NRziC/tjiHcPAFmU2MdAQKBgAFGCVGRwZisRMIe2N9zWztPOdVhZjwBC/KH+3rW
ouzYrRJDxM4EySz4q6+2nl0cMqysp8BOarpdCx+WwU9mO9Uts3GM77xmph3WL0Rh
XCOTeHq5GWUBbGZb2v6TOWQdQx94hqru+EKUT7J9qylkjqsS/9kyyPkabJspQs/b
HqcJAoGBAMEWNC5dKg8UDru0l1YXwY1Qxw2MzS0KubwnTbNMQFwnpsRtnC0pB92F
WucvI5wd9iLxweeRsLGRKuq8/wSr1+QWDW/yLEFx8ZK2k2nzpZajZ8KnXBK8L85H
ksj8ltZI8axKMcSrIYMpedG85m1KptAS84IKlaGXmqI1YJ449iUy
-----END RSA PRIVATE KEY-----"""
SSH_PUBLK="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCiMG3yBvxKzoSO9/5JOnhrfgNq3e6KpNal/uwjhanINE9S/GXJmvpW5HxwHz9q95+7UyzeD5je4ZujJ1CDSrKhqWrjlU2a0O+vCkYd1oONbOG1SHIWE0Lv2kiJ1e4oBboYmnrl4uwu6qLdtM86fxAZvW0uLUfIPyy8IAEnmy3YID/wx1b01wiZTCfKvRJYDMio+J+/isTM5byD216L2h1UDIqDbyNHt/DWoZMSYdZfW4Pmdeb9wC63U8PPJxF3h2BItLk0WyvRcSIZtd8+n1s7zYWLCcgA5Erel5LYVnnMy+ita3DBqRZHLWCAaTZzlXChxIm9ISZxUqtzOrCH+BUZ student@ccst9015-rpi"

#
# POPULATION AND/OR UPDATE HANDLING ROUTINE
#
for i in `seq 0 $((${#REPO_SEVR[@]} - 1))`; do
    if [ ${#PROJ_NAME[@]} -eq 0 ]; then
        ITER_NAME="$PROJ_PRFX$((i+1))";
    else
        if [ -z ${PROJ_NAME[i]} ]; then
            ITER_NAME="$PROJ_PRFX$((i+1))";
        else
            ITER_NAME="${PROJ_NAME[i]}";
        fi;
    fi;

    #
    # PREPARE SSH KEY FOR GIT SSH HANDSHAKE
    #
    if [ ${#SSHK_FILE[@]} -eq 0 ] || [ -z ${SSHK_FILE[i]} ]; then
        keyf=`/bin/mktemp`;

        /bin/echo "$SSH_PRIVK" | /usr/bin/tee "$keyf" > /dev/null;
    else
        /bin/echo "[NOTE] Using Separate SSH Key";

        keyf="${SSHK_FILE[i]}";
    fi;

    chmod 600 "$keyf" && chown "$PRIV_USER" "$keyf";
    #
    # (END OF) PREPARE SSH KEY FOR GIT SSH HANDSHAKE
    #

    # [WARNING] The script is expect to be ran at root privilege.
    #
    PROJ_BASE="$PROJ_BASE" PROJ_NAME="$ITER_NAME" REPO_SEVR="${REPO_SEVR[i]}" REPO_USER="${REPO_USER[i]}" REPO_NAME="${REPO_NAME[i]}" REPO_BRCH="${REPO_BRCH[i]}" SSHK_FILE="$keyf" "$ACTN_SCPT" 2>&1 | while read line; do
        echo "$ITER_NAME: $line";
    done;

    #
    # CLEAN UP SSH KEY USED FOR GIT SSH HANDSHAKE
    #
    if [ -z ${SSHK_FILE[i]} ]; then
        /bin/rm "$keyf";
    fi;
    #
    # (END OF) CLEAN UP SSH KEY USED FOR GIT SSH HANDSHAKE
    #

    #
    # POPULATE LIBRARY DIRECTORY PATH
    #
    if [ `cat /etc/profile | grep "^export PYTHONPATH=\"\\\$PYTHONPATH:${PROJ_BASE}/${ITER_NAME}" | wc -l` -eq 0 ]; then
        echo "export PYTHONPATH=\"\$PYTHONPATH:${PROJ_BASE}/${ITER_NAME}\"" | tee -a /etc/profile;
    fi;
    #
    # (END OF) POPULATE LIBRARY DIRECTORY PATH
    #
done;
#
# (END OF) POPULATION AND/OR UPDATE HANDLING ROUTINE
#