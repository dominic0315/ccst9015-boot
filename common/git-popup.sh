#!/bin/bash

#
# Name: git-popup.sh
#
# Author: C.-H. Dominic HUNG <dominic@teli.hku.hk / chdhung@hku.hk>
#  Technology-Enriched Learning Initiative,
#  The University of Hong Kong
#
# Description: This script pre-populates and/or updates local repositories of
#  Git-enabled projects from remote repository.
#
#  The script is developed for the course CCST9015 offered in Spring 2019.
#
#  [WARNING] This script should be ran at non-root privilege to relieve works on
#  user-ownership and user-permission issues for obtained files.
#

# PARAMETERS
# ====================
#
# PROJ_BASE="/home/student/Classwork"   # TO BE RETRIEVED FROM ENVIRONMENTAL VARIABLE
# PROJ_NAME="CW3"                       # TO BE RETRIEVED FROM ENVIRONMENTAL VARIABLE
# REPO_SEVR="bitbucket.org"             # TO BE RETRIEVED FROM ENVIRONMENTAL VARIABLE
# REPO_USER="dominic0315"               # TO BE RETRIEVED FROM ENVIRONMENTAL VARIABLE
# REPO_NAME="ccst9015-speech"           # TO BE RETRIEVED FROM ENVIRONMENTAL VARIABLE
# SSHK_FILE="/home/student/.ssh/id_rsa" # TO BE RETRIEVED FROM ENVIRONMENTAL VARIABLE

#
# PRE-REQUISITE VARIABLES CHECK
#
if [ -z ${PROJ_BASE+x} ] || [ -z ${PROJ_NAME+x} ] || [ -z ${REPO_SEVR+x} ] || [ -z ${REPO_USER+x} ] || [ -z ${REPO_NAME+x} ] || [ -z ${SSHK_FILE+x} ] \
    || [ -z "$PROJ_BASE" ] || [ -z "$PROJ_NAME" ] || [ -z "$REPO_SEVR" ] || [ -z "$REPO_USER" ] || [ -z "$REPO_NAME" ] || [ -z "$SSHK_FILE" ]; then
    /bin/echo "[ERROR] Pre-requisite Variable Check Failed. Now Exit." && exit 1;
fi;

/bin/echo "[NOTE] Pre-requisite Variable Check Passed";
#
# (END OF) PRE-REQUISITE VARIABLES CHECK
#

#
# POPULATE OR UPDATE LOCAL CLASSWORK REPOSITORY
#
/bin/echo "[NOTE] Git Repository at $PROJ_BASE/$PROJ_NAME is to be Updated/Populated";

if [ ! -e "$PROJ_BASE" ]; then
    mkdir -p "$PROJ_BASE";
fi

cd "$PROJ_BASE";

if [ ! -e "$PROJ_BASE/$PROJ_NAME" ]; then
    GIT_SSH_COMMAND="/usr/bin/ssh -o \"UserKnownHostsFile=/dev/null\" -o \"StrictHostKeyChecking=no\" -i \"$SSHK_FILE\"" /usr/bin/git clone "git@$REPO_SEVR:$REPO_USER/$REPO_NAME.git" "$PROJ_BASE/$PROJ_NAME" 2>&1;
fi;

cd "$PROJ_BASE/$PROJ_NAME" || { /bin/echo "[ERROR] Local Git Repository Not Found. Now Exit." && exit 128; };

/usr/bin/git rev-parse --is-inside-work-tree && GIT_SSH_COMMAND="/usr/bin/ssh -o \"UserKnownHostsFile=/dev/null\" -o \"StrictHostKeyChecking=no\" -i \"$SSHK_FILE\"" /usr/bin/git pull 2>&1 || { /bin/echo "[ERROR] Local Git Repository Not Found. Now Exit." && exit 127; };

if [ ! -z "$REPO_BRCH" ]; then
    /bin/echo "[NOTE] Checking Out $REPO_BRCH Branch";

    /usr/bin/git checkout "$REPO_BRCH" 2>&1;
fi;

/bin/echo "[NOTE] Git Repository at $PROJ_BASE/$PROJ_NAME Update/Population Completed";
#
# (END OF) POPULATE OR UPDATE LOCAL CLASSWORK REPOSITORY
#