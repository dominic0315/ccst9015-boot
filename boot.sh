#!/bin/bash

#
# Name: boot.sh
#
# Author: C.-H. Dominic HUNG <dominic@teli.hku.hk / chdhung@hku.hk>
#  Technology-Enriched Learning Initiative,
#  The University of Hong Kong
#
# Description: This script populates and/or updates course material and/or
#  perform system administration and/or maintenance tasks stipulated by system
#  administrator on execution. It checks against a centralised location, i.e., a
#  Git repository, that deposes scripts and data to be executed. At running, the
#  script pulls updates from such Git repository and for every scripts found, it
#  check against an include lists, ignore lists and history list to determine if
#  a script should be executed.
#
#  1) include lists, i.e., .include and .include.local should reside the file
#      name of each scripts that should always be ran.
#  2) ignore lists, i.e., .ignore and .ignore.local, should reside the file name
#      of each scripts that should always be skipped.
#  3) history list, i.e., .history, is an automatically maintained list that
#      upkeep a history of ran scripts and their exit status code for debug. If
#      a specific script is in neither include list nor ignore list mentioned
#      above, it is assumed an one-time script and will not be executed again
#      if such script is found in this history list. The modification of the
#      list should solely be the responsibility of this script to apprehend
#      erroneous behaviour due to mis-handling.
#
#  The .local suffixed of the above lists file are local blacklist and whitelist
#  for local operation. The other blacklist and whitelist files should not be
#  touched as modifications on such files renders Git update to fail and updated
#  Git-maintained blacklist and whitelist will not be crawled to local machine.
#
#  The script is expected to be ran at boot time by cron with root privilege.
#  Scripts to be ran by this script should be set with executable permission
#  accordingly when committing to the Git repository.
#
#  This script is developed for and shipped with the Raspberry Pi distributed
#  to students of the course CCST9015 offered in Spring 2019.
#

# PARAMETERS
# ====================
#
FILE_NAME="boot.sh"
HIST_NAME=".history"
INCL_NAME=".include"
IGNR_NAME=".ignore"
ETC_DIR="/etc"
LOG_DIR="/var/log/ccst9015"
REPO_SEVR="bitbucket.org"
REPO_USER="dominic0315"
DATE_FRMT="%b %d %H:%M:%S"
SSHK_FILE= # If a path to SSH key file, e.g., /etc/id_rsa, is provided through
           #  this variable, the default key interleaved below will not be used.
SSH_PRIVK="""-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAojBt8gb8Ss6Ejvf+STp4a34Dat3uiqTWpf7sI4WpyDRPUvxl
yZr6VuR8cB8/avefu1Ms3g+Y3uGboydQg0qyoalq45VNmtDvrwpGHdaDjWzhtUhy
FhNC79pIidXuKAW6GJp65eLsLuqi3bTPOn8QGb1tLi1HyD8svCABJ5st2CA/8MdW
9NcImUwnyr0SWAzIqPifv4rEzOW8g9tei9odVAyKg28jR7fw1qGTEmHWX1uD5nXm
/cAut1PDzycRd4dgSLS5NFsr0XEiGbXfPp9bO82FiwnIAORK3peS2FZ5zMvorWtw
wakWRy1ggGk2c5VwocSJvSEmcVKrczqwh/gVGQIDAQABAoIBAGwMb3JpjPIZGt5U
ACewuECENqqfJydeIVF8vrv7c9xYXPAgDX99q4DvvqbDanBMfzFAfAeDxIfXLORI
cK7GuLJSN09eS9DzmHm4EnYWTpWND+DBXp+uPU2k0eZhisNG7W8umulDf6zRy6Z/
qf6CX6uGMpIswEF7NMYvXBWSn7v20KJ4lk1eZntJ1N3lTP+TjhRu4XjE4tzv2+oZ
I5X4s9ax+D3MwsKDaUTAOWOvsiJIOshQ5gy+eR0Tlm+g4e4vl5++59jL0UqsggxP
BNELEjV9D+z/7iS50KGF9QE80MQPoOhSuqTxDeqajsI56xvV7KIIdu+LfZbLdcNh
MvAKcAECgYEAzHOqVwo1810X3a8Q/N/xHSJoUiZCI4i8ZJgK1A556pzlsM/RIn/q
kiHYBxXYWPmpOKjXzvrFInXe7GR/UcY2eCk2woZ6hTyY+f12F2dk3cFer40ytnoJ
qDOtZOGEZEq8CtR3g7wtYH++4Ki+oLL7XOLM8lqBTb/p0DoHDhhv14ECgYEAyxTs
cggzV72fExAwxPhT4SQ3MLqfFVpJPF/t0znHnpVwFHSFKXo0K4FYEGJ2LTJRhEEL
MBDWwPKZnsGmZxzL74PQMhD+9uWJ9PnV/yUSpXDGMZEzw3fYACi05pk0iDFaxdq2
yZWcD04H+WBUIDbZxn0mHkrZ724RLqHuAnLsyZkCgYBmJKtHjPxjN3FC+cG89Izb
h2MjF6YXZqrofuV6CDl0syo16b2MROgGVg49i/ZojWKzwfb/m1191/GlnJfXXPW9
lqmbCeCQTDQJQU4MZ0WHXKJXhm19qP7GH/tS2TBxgrzrlDaZhBwPiALu7p8+Xe0i
/NRziC/tjiHcPAFmU2MdAQKBgAFGCVGRwZisRMIe2N9zWztPOdVhZjwBC/KH+3rW
ouzYrRJDxM4EySz4q6+2nl0cMqysp8BOarpdCx+WwU9mO9Uts3GM77xmph3WL0Rh
XCOTeHq5GWUBbGZb2v6TOWQdQx94hqru+EKUT7J9qylkjqsS/9kyyPkabJspQs/b
HqcJAoGBAMEWNC5dKg8UDru0l1YXwY1Qxw2MzS0KubwnTbNMQFwnpsRtnC0pB92F
WucvI5wd9iLxweeRsLGRKuq8/wSr1+QWDW/yLEFx8ZK2k2nzpZajZ8KnXBK8L85H
ksj8ltZI8axKMcSrIYMpedG85m1KptAS84IKlaGXmqI1YJ449iUy
-----END RSA PRIVATE KEY-----"""
SSH_PUBLK="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCiMG3yBvxKzoSO9/5JOnhrfgNq3e6KpNal/uwjhanINE9S/GXJmvpW5HxwHz9q95+7UyzeD5je4ZujJ1CDSrKhqWrjlU2a0O+vCkYd1oONbOG1SHIWE0Lv2kiJ1e4oBboYmnrl4uwu6qLdtM86fxAZvW0uLUfIPyy8IAEnmy3YID/wx1b01wiZTCfKvRJYDMio+J+/isTM5byD216L2h1UDIqDbyNHt/DWoZMSYdZfW4Pmdeb9wC63U8PPJxF3h2BItLk0WyvRcSIZtd8+n1s7zYWLCcgA5Erel5LYVnnMy+ita3DBqRZHLWCAaTZzlXChxIm9ISZxUqtzOrCH+BUZ student@ccst9015-rpi"

#
# PREPARATORY WORKS
#
/bin/mkdir -p "$LOG_DIR";

/bin/echo "$(/bin/date "+$DATE_FRMT") CCST9015 Update/Population" | /usr/bin/tee -a "$LOG_DIR/$FILE_NAME.log";

ping -c 1 8.8.8.8 && host "$REPO_SEVR";

while [ $? -ne 0 ]; do
    ping -c 1 8.8.8.8 && host "$REPO_SEVR";
done;

tmpf=`/bin/mktemp`;

/usr/bin/ssh-keygen -B -f "$tmpf";

while [ $? -ne 0 ]; do
    /usr/bin/ssh-keyscan "$REPO_SEVR" | /usr/bin/tee "$tmpf";

    /usr/bin/ssh-keygen -B -f "$tmpf";
done;

/bin/rm "$tmpf";

/bin/echo "$(/bin/date "+$DATE_FRMT") CORE: Network Connection Established" | /usr/bin/tee -a "$LOG_DIR/$FILE_NAME.log";
#
# (END OF) PREPARATORY WORKS
#

#
# GENERAL STARTUP SCRIPTS HANDLING
#
HIST_FILE="$ETC_DIR/ccst9015.d/$HIST_NAME";
INCL_FILE="$ETC_DIR/ccst9015.d/$INCL_NAME";
IGNR_FILE="$ETC_DIR/ccst9015.d/$IGNR_NAME";

cd "$ETC_DIR";

if [ -z "$SSHK_FILE" ]; then
    keyf=`/bin/mktemp`;

    /bin/echo "$SSH_PRIVK" | /usr/bin/tee "$keyf" > /dev/null;
else
    /bin/echo "$(/bin/date "+$DATE_FRMT") CORE: Using Separate SSH Key" | /usr/bin/tee -a "$LOG_DIR/$FILE_NAME.log";

    keyf="$SSHK_FILE";
fi;

if [ ! -e "$ETC_DIR/ccst9015.d" ]; then
    GIT_SSH_COMMAND="/usr/bin/ssh -o \"UserKnownHostsFile=/dev/null\" -o \"StrictHostKeyChecking=no\" -i \"$keyf\"" /usr/bin/git clone "git@$REPO_SEVR:$REPO_USER/ccst9015-boot.git" "$ETC_DIR/ccst9015.d" 2>&1 | while read line; do
        /bin/echo "$(/bin/date "+$DATE_FRMT") $line";
    done | /usr/bin/tee -a "$LOG_DIR/git.log";

    /bin/echo "$FILE_NAME" | /usr/bin/tee -a "$IGNR_FILE.local" > /dev/null;
fi;

cd "$ETC_DIR/ccst9015.d" || { /bin/echo "$(/bin/date "+$DATE_FRMT") CORE: [ERROR] Local Git Repository Not Found. Now Exit." | /usr/bin/tee -a "$LOG_DIR/$FILE_NAME.log" && exit 128; };

/usr/bin/git rev-parse --is-inside-work-tree && GIT_SSH_COMMAND="/usr/bin/ssh -o \"UserKnownHostsFile=/dev/null\" -o \"StrictHostKeyChecking=no\" -i \"$keyf\"" /usr/bin/git pull 2>&1 | while read line; do
    /bin/echo "$(/bin/date "+$DATE_FRMT") $line";
done | /usr/bin/tee -a "$LOG_DIR/git.log" || { /bin/echo "$(/bin/date "+$DATE_FRMT") CORE: [ERROR] Local Git Repository Not Found. Now Exit." | /usr/bin/tee -a "$LOG_DIR/$FILE_NAME.log" && exit 127; };

if [ -z "$SSHK_FILE" ]; then
    /bin/rm "$keyf";
fi;

#
# [WARNING] The file permission should be set to executable when committing the
#  to-run scripts in the Git repository.
#
for scpt in `/bin/ls *.sh`; do
    if [ `/bin/cat "$INCL_FILE" "$INCL_FILE.local" | /bin/grep "$scpt" | /usr/bin/wc -l` -eq 0 ] && { [ `/bin/cat "$IGNR_FILE" "$IGNR_FILE.local" | /bin/grep "$scpt" | /usr/bin/wc -l` -gt 0 ] || [ `/bin/cat "$HIST_FILE" | /bin/grep "$scpt" | /usr/bin/wc -l` -gt 0 ]; }; then
        :
    else
        /bin/echo " CORE: Dispatching $scpt To Execution";

        (shopt -s -o pipefail; { . $scpt 2>&1 | /usr/bin/tee /dev/stderr 2>&3 | while read line; do
            /bin/echo "$(/bin/date "+$DATE_FRMT") $line";
        done | /usr/bin/tee -a "$LOG_DIR/$scpt.log" > /dev/null; } 3>&1 | while read line; do
            /bin/echo "   $scpt: $line";
        done;);

        scpt_exit=$?;

        /bin/echo "$(/bin/date "+$DATE_FRMT") $scpt $scpt_exit" | /usr/bin/tee -a "$HIST_FILE" > /dev/null;

        /bin/echo " CORE: $scpt Execution Completed";
    fi;
done | while read line; do
    /bin/echo "$(/bin/date "+$DATE_FRMT") $line";
done | /usr/bin/tee -a "$LOG_DIR/$FILE_NAME.log";

/bin/echo "$(/bin/date "+$DATE_FRMT") CORE: Update/Population Completed" | /usr/bin/tee -a "$LOG_DIR/$FILE_NAME.log";

#
# (END OF) GENERAL STARTUP SCRIPTS HANDLING
#